unit VendaEnt;

interface

uses
   System.Generics.Collections, System.SysUtils, VendaItemEnt,
   MVCFramework.Serializer.Commons;
type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TVendaEnt = class
  private
    FCodVenda: Integer;
    FCodCli: Integer;
    FDataVenda: TDateTime;
    FValorTotVenda: Double;
    FListaVendaItem: TObjectList<TVendaItemEnt>;
  public
    constructor Create(ListaVendaItem: TObjectList<TVendaItemEnt>)  overload;
    constructor Create()  overload;
    destructor Destroy; override;

    // todas as propriedades abaixo de preferencia minusculas (exemplo: "codvenda")
    property codvenda: Integer read FCodVenda write FCodVenda;
    property codcli: Integer read FCodCli write FCodCli;
    property datavenda: TDateTime read FDataVenda write FDataVenda;
    property valortotvenda: Double read FValorTotVenda write FValorTotVenda;
    [MVCListOf(TVendaItemEnt)] // comando para "trabalhar com listagens
    property listavendaitem: TObjectList<TVendaItemEnt> read FListaVendaItem
                                                  write FListaVendaItem;


  end;

implementation

{ TVendaEnt }

constructor TVendaEnt.Create(ListaVendaItem: TObjectList<TVendaItemEnt>);
begin
  FListaVendaItem := ListaVendaItem;
end;

constructor TVendaEnt.Create;
begin
  FListaVendaItem := TObjectList<TVendaItemEnt>.Create();
end;

destructor TVendaEnt.Destroy;
begin
//  if Assigned(FListaVendaItem) then
//    FListaVendaItem.Free;
  inherited;
end;

{ TVendaEnt }



end.
