unit controller1;

interface

uses
  MVCFramework, MVCFramework.Commons;

type

  [MVCPath('/api')]
  TMyController = class(TMVCController)
  public
    [MVCPath('/')]
    [MVCHTTPMethod([httpGET])]
    procedure Index;

    [MVCPath('/reversedstrings/($Value)')]
    [MVCHTTPMethod([httpGET])]
    procedure GetReversedString(const Value: String);
  protected
    procedure OnBeforeAction(Context: TWebContext; const AActionName: string; var Handled: Boolean); override;
    procedure OnAfterAction(Context: TWebContext; const AActionName: string); override;

  public
    //Sample CRUD Actions for a "Customer" entity
    [MVCPath('/customers')]
    [MVCHTTPMethod([httpGET])]
    procedure GetCustomers;

    [MVCPath('/customers/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure GetCustomer(id: Integer);

    [MVCPath('/customers')]
    [MVCHTTPMethod([httpPOST])]
    procedure CreateCustomer;

    [MVCPath('/customers/($id)')]
    [MVCHTTPMethod([httpPUT])]
    procedure UpdateCustomer(id: Integer);

    [MVCPath('/customers/($id)')]
    [MVCHTTPMethod([httpDELETE])]
    procedure DeleteCustomer(id: Integer);


//////////////////////////////////////////////////////////////////////////


    [MVCPath('/produtos')]
    [MVCHTTPMethod([httpGET])]
    procedure GetProdutos;

    [MVCPath('/produto/($Value)')]
    [MVCHTTPMethod([httpGET])]
    procedure GetProduto(const Value: String);

    [MVCPath('/insereprodteste')]
    [MVCHTTPMethod([httpPOST])]
    procedure insereprodteste(ctx: TWebContext);

    //

    [MVCPath('/procuraprod/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraProd(id: integer);

    [MVCPath('/procuraprods')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraProds;

    [MVCPath('/procuraprodtipo/($idtipo)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraProdTipo(idtipo: integer);

    [MVCPath('/insereprod')]
    [MVCHTTPMethod([httpPOST])]
    procedure InsereProd;

    [MVCPath('/excluiprod/($id)')]
    [MVCHTTPMethod([httpDELETE])]
    procedure ExcluiProd(id: integer);

    [MVCPath('/alteraprod/($id)')]
    [MVCHTTPMethod([httpPUT])]
    procedure AlteraProd(id: integer);

    //

    [MVCPath('/procuravenda/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraVenda(id: integer);

    [MVCPath('/procuravendas/($dataini)/($datafim)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraVendas(dataini, datafim: TDate);

    [MVCPath('/procuravendas2')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraVendas2;

    [MVCPath('/inserevenda')]
    [MVCHTTPMethod([httpPOST])]
    procedure InsereVenda;

    [MVCPath('/alteravenda')]
    [MVCHTTPMethod([httpPOST])]
    procedure AlteraVenda;


    //

    [MVCPath('/procuratipoprod/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraTipoProd(id: integer);

    [MVCPath('/procuratipoprods')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraTipoProds;

    [MVCPath('/inseretipoprod')]
    [MVCHTTPMethod([httpPOST])]
    procedure InsereTipoProd;

    [MVCPath('/excluitipoprod/($id)')]
    [MVCHTTPMethod([httpDELETE])]
    procedure ExcluiTipoProd(id: integer);

    [MVCPath('/alteratipoprod/($id)')]
    [MVCHTTPMethod([httpPUT])]
    procedure AlteraTipoProd(id: integer);


    //

    [MVCPath('/procuraformapagto/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraFormaPagto(id: integer);

    [MVCPath('/procuraformaPagtos')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraFormaPagtos;

    [MVCPath('/insereformapagto')]
    [MVCHTTPMethod([httpPOST])]
    procedure InsereFormaPagto;

    [MVCPath('/excluiformapagto/($id)')]
    [MVCHTTPMethod([httpDELETE])]
    procedure ExcluiFormaPagto(id: integer);

    [MVCPath('/alteraformapagto')]
    [MVCHTTPMethod([httpPOST])]
    procedure AlteraFormaPagto;

    //

    [MVCPath('/procurapedido/($id)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraPedido(id: integer);

    [MVCPath('/procurapedidos/($dataini)/($datafim)')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraPedidos(dataini, datafim: TDate);

    [MVCPath('/procurapedidos2')]
    [MVCHTTPMethod([httpGET])]
    procedure ProcuraPedidos2;

    [MVCPath('/inserepedido')]
    [MVCHTTPMethod([httpPOST])]
    procedure InserePedido;

    [MVCPath('/alterapedido')]
    [MVCHTTPMethod([httpPOST])]
    procedure AlteraPedido;

  end;

implementation

uses
  System.SysUtils, MVCFramework.Logger, System.StrUtils, model_produtos,
  ProdutoEnt, ProdutoMod, VendaEnt, VendaItemEnt, VendaMod, TipoProdEnt,
  TipoProdMod, FormaPagtoEnt, FormaPagtoMod, PedidoEnt, PedidoMod;

procedure TMyController.Index;
begin
  //use Context property to access to the HTTP request and response
  Render('Hello DelphiMVCFramework World');
end;

procedure TMyController.GetReversedString(const Value: String);
begin
  Render(System.StrUtils.ReverseString(Value.Trim));
end;

procedure TMyController.OnAfterAction(Context: TWebContext; const AActionName: string);
begin
  { Executed after each action }
  inherited;
end;

procedure TMyController.OnBeforeAction(Context: TWebContext; const AActionName: string; var Handled: Boolean);
begin
  { Executed before each action
    if handled is true (or an exception is raised) the actual
    action will not be called }
  inherited;

  if AActionName='GetCustomers' then
    if Context.Request.Headers['token']='admin' then
     Handled:= true
    else
    begin
         Handled:= False;
         Render(403,'Erro autenticação');
    end;

end;

//Sample CRUD Actions for a "Customer" entity
procedure TMyController.GetCustomers;
begin
  //todo: render a list of customers
end;

procedure TMyController.GetCustomer(id: Integer);
begin
  //todo: render the customer by id
end;

procedure TMyController.CreateCustomer;

begin
  //todo: create a new customer
end;

procedure TMyController.UpdateCustomer(id: Integer);
begin
  //todo: update customer by id
end;

procedure TMyController.DeleteCustomer(id: Integer);
begin
  //todo: delete customer by id
end;


procedure TMyController.GetProduto(const Value: String);
begin
   try
     Render(TProduto.Get(StrToIntDef(Value,0)),true);
   finally

   end;
end;

procedure TMyController.GetProdutos;
begin
   try
     Render<TProduto>(TProduto.GetAll);
   finally

   end;
end;


procedure TMyController.insereprodTeste(ctx: TWebContext);
var
  httpStatus:Integer;
begin
     httpStatus := TProduto.insereprod(Context.Request.BodyAs<TProduto>,Context.Request.Body);

     Render(httpStatus,'');
end;

///////////////////////////////////


procedure TMyController.ProcuraProd(id: integer);
var
  ProdutoDAO: TProdutoDAO;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    Render(ProdutoDAO.ProcuraProdID(id));
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.ProcuraProds;
var
  ProdutoDAO: TProdutoDAO;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    Render<TProdutoEnt>(ProdutoDAO.ProcuraTodosProd);
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.ProcuraProdTipo(idtipo: integer);
var
  ProdutoDAO: TProdutoDAO;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    Render<TProdutoEnt>(ProdutoDAO.ProcuraTodosProdTipo(idtipo));
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.InsereProd;
var
  ProdutoDAO: TProdutoDAO;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    ProdutoDAO.InsereProd(Context.Request.BodyAs<TProdutoEnt>);
    Render('{"Result":"insereprod2"}');
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.ExcluiProd(id: integer);
var
  ProdutoDAO: TProdutoDAO;
  iCodRetorno: integer;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    iCodRetorno := ProdutoDAO.ExcluiProd(id);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.AlteraProd(id: integer);
var
  ProdutoDAO: TProdutoDAO;
  iCodRetorno: integer;
begin
  ProdutoDAO := TProdutoDAO.Create();
  try
    iCodRetorno := ProdutoDAO.AlteraProd(id, Context.Request.BodyAs<TProdutoEnt>);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    ProdutoDAO.Free;
  end;
end;

procedure TMyController.ProcuraVenda(id: integer);
var
  VendaDAO: TVendaDAO;
begin
  VendaDAO := TVendaDAO.Create();
  try
    Render(VendaDAO.ProcuraVenda(id));
  finally
    VendaDAO.Free;
  end;
end;

procedure TMyController.ProcuraVendas(dataini, datafim: TDate);
var
  VendaDAO: TVendaDAO;
begin
  VendaDAO := TVendaDAO.Create();
  try
    Render<TVendaEnt>(VendaDAO.ProcuraVendas(dataini, datafim));
  finally
    VendaDAO.Free;
  end;
end;

procedure TMyController.ProcuraVendas2;
var
  VendaDAO: TVendaDAO;
  DataIni, DataFim: TDate;
  sData: string;
begin
  VendaDAO := TVendaDAO.Create();

  sData   := Copy((Context.Request.Params['dataini']),9,2)+'/'+Copy((Context.Request.Params['dataini']),6,2)+'/'+Copy((Context.Request.Params['dataini']),1,4);
  DataIni := StrToDate(sData);
  sData   := Copy((Context.Request.Params['datafim']),9,2)+'/'+Copy((Context.Request.Params['datafim']),6,2)+'/'+Copy((Context.Request.Params['datafim']),1,4);
  DataFim := StrToDate(sData);

  try
    Render<TVendaEnt>(VendaDAO.ProcuraVendas(DataIni, DataFim));
  finally
    VendaDAO.Free;
  end;
end;

procedure TMyController.InsereVenda;
var
  VendaDAO: TVendaDAO;
begin
  VendaDAO := TVendaDAO.Create();
  try
    VendaDAO.InsereVenda(Context.Request.BodyAs<TVendaEnt>);
    Render('{"Result":"inserevenda"}');
  finally
    VendaDAO.Free;
  end;
end;

procedure TMyController.AlteraVenda;
var
  VendaDAO: TVendaDAO;
begin
  VendaDAO := TVendaDAO.Create();
  try
    VendaDAO.AlteraVenda(Context.Request.BodyAs<TVendaEnt>);
    Render('{"Result":"alteravenda"}');
  finally
    VendaDAO.Free;
  end;
end;

procedure TMyController.ProcuraTipoProd(id: integer);
var
  TipoProdDAO: TTipoProdDAO;
begin
  TipoProdDAO := TTipoProdDAO.Create();
  try
    Render(TipoProdDAO.ProcuraTipoProdID(id));
  finally
    TipoProdDAO.Free;
  end;
end;

procedure TMyController.ProcuraTipoProds;
var
  TipoProdDAO: TTipoProdDAO;
begin
  TipoProdDAO := TTipoProdDAO.Create();
  try
    Render<TTipoProdEnt>(TipoProdDAO.ProcuraTodosTipoProd);
  finally
    TipoProdDAO.Free;
  end;
end;

procedure TMyController.InsereTipoProd;
var
  TipoProdDAO: TTipoProdDAO;
begin
  TipoProdDAO := TTipoProdDAO.Create();
  try
    TipoProdDAO.InsereTipoProd(Context.Request.BodyAs<TTipoProdEnt>);
    Render('{"Result":"inseretipoprod2"}');
  finally
    TipoProdDAO.Free;
  end;
end;

procedure TMyController.ExcluiTipoProd(id: integer);
var
  TipoProdDAO: TTipoProdDAO;
  iCodRetorno: integer;
begin
  TipoProdDAO := TTipoProdDAO.Create();
  try
    iCodRetorno := TipoProdDAO.ExcluiTipoProd(id);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    TipoProdDAO.Free;
  end;
end;

procedure TMyController.AlteraTipoProd(id: integer);
var
  TipoProdDAO: TTipoProdDAO;
  iCodRetorno: integer;
begin
  TipoProdDAO := TTipoProdDAO.Create();
  try
    iCodRetorno := TipoProdDAO.AlteraTipoProd(id, Context.Request.BodyAs<TTipoProdEnt>);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    TipoProdDAO.Free;
  end;
end;

procedure TMyController.ProcuraFormaPagto(id: integer);
var
  FormaPagtoDAO: TFormaPagtoDAO;
begin
  FormaPagtoDAO := TFormaPagtoDAO.Create();
  try
    Render(FormaPagtoDAO.ProcuraFormaPagtoID(id));
  finally
    FormaPagtoDAO.Free;
  end;
end;

procedure TMyController.ProcuraFormaPagtos;
var
  FormaPagtoDAO: TFormaPagtoDAO;
begin
  FormaPagtoDAO := TFormaPagtoDAO.Create();
  try
    Render<TFormaPagtoEnt>(FormaPagtoDAO.ProcuraTodosFormaPagto);
  finally
    FormaPagtoDAO.Free;
  end;
end;

procedure TMyController.InsereFormaPagto;
var
  FormaPagtoDAO: TFormaPagtoDAO;
begin
  FormaPagtoDAO := TFormaPagtoDAO.Create();
  try
    FormaPagtoDAO.InsereFormaPagto(Context.Request.BodyAs<TFormaPagtoEnt>);
    Render('{"Result":"insereformapagto"}');
  finally
    FormaPagtoDAO.Free;
  end;
end;

procedure TMyController.ExcluiFormaPagto(id: integer);
var
  FormaPagtoDAO: TFormaPagtoDAO;
  iCodRetorno: integer;
begin
  FormaPagtoDAO := TFormaPagtoDAO.Create();
  try
    iCodRetorno := FormaPagtoDAO.ExcluiFormaPagto(id);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    FormaPagtoDAO.Free;
  end;
end;

procedure TMyController.AlteraFormaPagto;
var
  FormaPagtoDAO: TFormaPagtoDAO;
  iCodRetorno: integer;
begin
  FormaPagtoDAO := TFormaPagtoDAO.Create();
  try
    iCodRetorno := FormaPagtoDAO.AlteraFormaPagto(Context.Request.BodyAs<TFormaPagtoEnt>);
    Render('{"Result":"' +inttostr(iCodRetorno)+ '"}');
  finally
    FormaPagtoDAO.Free;
  end;
end;

//

procedure TMyController.ProcuraPedido(id: integer);
var
  PedidoDAO: TPedidoDAO;
begin
  PedidoDAO := TPedidoDAO.Create();
  try
    Render(PedidoDAO.ProcuraPedido(id));
  finally
    PedidoDAO.Free;
  end;
end;

procedure TMyController.ProcuraPedidos(dataini, datafim: TDate);
var
  PedidoDAO: TPedidoDAO;
begin
  PedidoDAO := TPedidoDAO.Create();
  try
    Render<TPedidoEnt>(PedidoDAO.ProcuraPedidos(dataini, datafim));
  finally
    PedidoDAO.Free;
  end;
end;

procedure TMyController.ProcuraPedidos2;
var
  PedidoDAO: TPedidoDAO;
  DataIni, DataFim: TDate;
  sData: string;
begin
  PedidoDAO := TPedidoDAO.Create();

  sData   := Copy((Context.Request.Params['dataini']),9,2)+'/'+Copy((Context.Request.Params['dataini']),6,2)+'/'+Copy((Context.Request.Params['dataini']),1,4);
  DataIni := StrToDate(sData);
  sData   := Copy((Context.Request.Params['datafim']),9,2)+'/'+Copy((Context.Request.Params['datafim']),6,2)+'/'+Copy((Context.Request.Params['datafim']),1,4);
  DataFim := StrToDate(sData);

  try
    Render<TPedidoEnt>(PedidoDAO.ProcuraPedidos(DataIni, DataFim));
  finally
    PedidoDAO.Free;
  end;
end;

procedure TMyController.InserePedido;
var
  PedidoDAO: TPedidoDAO;
begin
  PedidoDAO := TPedidoDAO.Create();
  try
    PedidoDAO.InserePedido(Context.Request.BodyAs<TPedidoEnt>);
    Render('{"Result":"inserePedido"}');
  finally
    PedidoDAO.Free;
  end;
end;

procedure TMyController.AlteraPedido;
var
  PedidoDAO: TPedidoDAO;
begin
  PedidoDAO := TPedidoDAO.Create();
  try
    PedidoDAO.AlteraPedido(Context.Request.BodyAs<TPedidoEnt>);
    Render('{"Result":"alteraPedido"}');
  finally
    PedidoDAO.Free;
  end;
end;

end.
