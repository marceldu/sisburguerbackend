unit TipoProdEnt;

interface

type
  [MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TTipoProdEnt = class
  private
    FCodTipoProd: Integer;
    FDescTipoProd: string;
  public
    // propriedade sempre minuscula (codtipoprod)
    property codtipoprod: Integer read FCodTipoProd write FCodTipoProd;
    property desctipoprod: string read FDescTipoProd write FDescTipoProd;
  end;

implementation

end.
