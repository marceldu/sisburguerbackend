unit PedidoProdAcaoEnt;

interface

type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TPedidoProdAcaoEnt = class
  private
    FCodPed: Integer;
    FCodSeqProd: Integer;
    FCodAcao: Integer;
    FDescAcao: String;
  public
    // todas as propriedades em minusculo
    property codped: Integer read FCodPed write FCodPed;
    property codseqprod: Integer read FCodSeqProd write FCodSeqProd;
    property codacao: Integer read FCodAcao write FCodAcao;
    property descacao: String read FDescAcao write FDescAcao;
  end;

implementation

end.
