unit PedidoProdIngEnt;

interface

type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TPedidoProdIngEnt = class
  private
    FCodPed: Integer;
    FCodSeqProd: Integer;
    FCodSeqIng: Integer;
    FCodProdIng: Integer;
    FDescProdIng: String;
    FValorUnitIng: Double;
    FQtdProdIng: Integer;
    FIngPadrao: Smallint;
  public
    // todas as propriedades em minusculo
    property codped: Integer read FCodPed write FCodPed;
    property codseqprod: Integer read FCodSeqProd write FCodSeqProd;
    property codseqing: Integer read FCodSeqIng write FCodSeqIng;
    property codproding: Integer read FCodProdIng write FCodProdIng;
    property descproding: String read FDescProdIng write FDescProdIng;
    property valoruniting: Double read FValorUnitIng write FValorUnitIng;
    property qtdproding: Integer read FQtdProdINg write FQtdProdIng;
    property ingpadrao: Smallint read FIngPadrao write FIngPadrao;
  end;

implementation

end.
