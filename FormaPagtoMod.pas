unit FormaPagtoMod;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger, FormaPagtoEnt;


type
  TFormaPagtoDAO = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraFormaPagtoID(PCodFormPagto: Integer): TFormaPagtoEnt;
    function ProcuraTodosFormaPagto: TObjectList<TFormaPagtoEnt>;
    function ExcluiFormaPagto(PCodFormPagto: Integer): integer;
    function InsereFormaPAgto(FormaPagtoEnt: TFormaPagtoEnt): Integer;
    function AlteraFormaPagto(FormaPagtoEnt: TFormaPagtoEnt): Integer;

  published
    { published declarations }
  end;


implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TProdutoMod }

function TFormaPagtoDAO.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

function TFormaPagtoDAO.ProcuraFormaPagtoID(PCodFormPagto: Integer): TFormaPagtoEnt;
var
  Obj:TFormaPagtoEnt;
  Query:TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    Query.Open('select * from formapagto where codformpagto = '+IntToStr(PCodFormPagto));

    Obj          := TFormaPagtoEnt.Create;
    Obj.codformpagto  := Query.FieldByName('codformpagto').AsInteger;
    Obj.descformpagto := Query.FieldByName('descformpagto').AsString;
  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= Obj;
end;

function TFormaPagtoDAO.ProcuraTodosFormaPagto: TObjectList<TFormaPagtoEnt>;
var
  FormaPagtoEnt: TFormaPagtoEnt;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  Result := TObjectList<TFormaPagtoEnt>.Create;
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from formapagto ');
    while not Query.Eof do
    begin
      FormaPagtoEnt            := TFormaPagtoEnt.Create;
      FormaPagtoEnt.CodFormPagto  := Query.FieldByName('codformpagto').Value;
      FormaPagtoEnt.DescFormPagto := Query.FieldByName('descformpagto').Value;
      Result.Add(FormaPagtoEnt);
      Query.Next;
    end;
  finally
  end;
end;

function TFormaPagtoDAO.ExcluiFormaPagto(PCodFormPagto: Integer): integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from formapagto where codformpagto = '+IntToStr(PCodFormPagto));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from formapagto where codformpagto = :codformpagto ';
        Command.Params.ParamByName('codformpagto').AsInteger := PCodFormPagto;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrato na FORMAPAGTO');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TFormaPagtoDAO.InsereFormaPagto(FormaPagtoEnt: TFormaPagtoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from formapagto where codformpagto = '+IntToStr(FormaPagtoEnt.codformpagto));
    if Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'insert into formapagto (codformpagto, descformpagto) values (:codformpagto, :descformpagto)';
        Command.Params.ParamByName('codformpagto').AsInteger    := FormaPagtoEnt.CodFormPagto;
        Command.Params.ParamByName('descformpagto').AsString    := FormaPagtoEnt.DescFormPagto;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na FORMAPAGTO');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TFormaPagtoDAO.AlteraFormaPagto(FormaPagtoEnt: TFormaPagtoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from formapagto where codformpagto = '+IntToStr(FormaPagtoEnt.codformpagto));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update formapagto set descformpagto = :descformpagto where codformpagto = :codformpagto';
        Command.Params.ParamByName('codformpagto').AsInteger := FormaPagtoEnt.CodFormPagto;
        Command.Params.ParamByName('descformpagto').AsString := FormaPagtoEnt.DescFormPagto;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na FORMAPAGTO');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

end.

