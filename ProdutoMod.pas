unit ProdutoMod;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger, ProdutoEnt;


type
  TProdutoDAO = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraProdID(PCodProd: Integer): TProdutoEnt;
    function ProcuraTodosProdTipo(PCodTipoProd: Integer): TObjectList<TProdutoEnt>;
    function ProcuraTodosProd: TObjectList<TProdutoEnt>;
    function ExcluiProd(PCodProd: Integer): integer;
    function InsereProd(ProdutoEnt: TProdutoEnt): Integer;
    function AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;

  published
    { published declarations }
  end;


implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TProdutoMod }

function TProdutoDAO.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

function TProdutoDAO.ProcuraProdID(PCodProd: Integer): TProdutoEnt;
var
  Obj:TProdutoEnt;
  Query:TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    Query.Open('select * from produto where codprod = '+IntToStr(PCodProd));

    Obj             := TProdutoEnt.Create;
    Obj.codprod     := Query.FieldByName('codprod').AsInteger;
    Obj.descprod    := Query.FieldByName('descprod').AsString;
    Obj.valunit     := Query.FieldByName('valunit').AsFloat;
    Obj.codtipoprod := Query.FieldByName('codtipoprod').AsInteger;
  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= Obj;
end;

function TProdutoDAO.ProcuraTodosProdTipo(PCodTipoProd: Integer): TObjectList<TProdutoEnt>;
var
  ProdutoEnt: TProdutoEnt;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  Result := TObjectList<TProdutoEnt>.Create;
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open('select * from produto where codtipoprod = '+IntToStr(PCodTipoProd)+' order by codprod ');
    while not Query.Eof do
    begin
      ProdutoEnt          := TProdutoEnt.Create;
      ProdutoEnt.CodProd  := Query.FieldByName('codprod').Value;
      ProdutoEnt.DescProd := Query.FieldByName('descprod').Value;
      ProdutoEnt.valunit  := Query.FieldByName('valunit').Value;
      ProdutoEnt.codtipoprod := Query.FieldByName('codtipoprod').Value;
      Result.Add(ProdutoEnt);
      Query.Next;
    end;
  finally
  end;
end;

function TProdutoDAO.ProcuraTodosProd: TObjectList<TProdutoEnt>;
var
  ProdutoEnt: TProdutoEnt;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  Result := TObjectList<TProdutoEnt>.Create;
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produto order by codprod ');
    while not Query.Eof do
    begin
      ProdutoEnt          := TProdutoEnt.Create;
      ProdutoEnt.CodProd  := Query.FieldByName('codprod').Value;
      ProdutoEnt.DescProd := Query.FieldByName('descprod').Value;
      ProdutoEnt.valunit  := Query.FieldByName('valunit').Value;
      ProdutoEnt.codtipoprod := Query.FieldByName('codtipoprod').Value;
      Result.Add(ProdutoEnt);
      Query.Next;
    end;
  finally
  end;
end;

function TProdutoDAO.ExcluiProd(PCodProd: Integer): integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produto where codprod = '+IntToStr(PCodProd));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from produto where codprod = :codprod ';
        Command.Params.ParamByName('codprod').AsInteger := PCodProd;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrato!');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TProdutoDAO.InsereProd(ProdutoEnt: TProdutoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produto where codprod = '+IntToStr(ProdutoEnt.codprod));
    if Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'insert into produto (codprod, descprod, valunit, codtipoprod) values (:codprod, :descprod, :valunit, :codtipoprod)';
        Command.Params.ParamByName('codprod').AsInteger     := ProdutoEnt.codprod;
        Command.Params.ParamByName('descprod').AsString     := ProdutoEnt.descprod;
        Command.Params.ParamByName('valunit').AsFloat       := ProdutoEnt.valunit;
        Command.Params.ParamByName('codtipoprod').AsInteger := ProdutoEnt.codtipoprod;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado!');
      Result := 204;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TProdutoDAO.AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produtos where codprod = '+IntToStr(iCodProdAnt));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update produtos set codprod = :codprodnov, descprod = :descprod where codprod = :codprodant';
        Command.Params.ParamByName('codprodnov').AsInteger := ProdutoEnt.CodProd;
        Command.Params.ParamByName('descprod').AsString    := ProdutoEnt.DescProd;
        Command.Params.ParamByName('codprodant').AsInteger := iCodProdAnt; // passo o codprod original caso precise troca-lo
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

end.

