unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Web.WebBroker, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef;

type
  TdmConexcao = class(TDataModule)
    FDConexao: TFDConnection;
    FDTransaction1: TFDTransaction;
    FDQuery1: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmConexcao: TdmConexcao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmConexcao.DataModuleCreate(Sender: TObject);
begin
  FDConexao.Close;
  //
  FDConexao.Params.Clear;
{
  // apontamento servidor amazon /////////////////////////
  FDConexao.Params.Add('Server=meuburguer2.cyn2tr3gj6gr.sa-east-1.rds.amazonaws.com');
  FDConexao.Params.Add('Database=meuburguer2');
  FDConexao.Params.Add('User_Name=marcel');
  FDConexao.Params.Add('Password=lecram01');
  FDConexao.Params.Add('DriverID=MySQL');
}
  // apontamento servidor local /////////////////////////
  FDConexao.Params.Add('Database=D:\marcel\projetos_pessoais\MVCFramework_New\Banco\DBMEUBURGUER.FDB');
  FDConexao.Params.Add('User_Name=SYSDBA');
  FDConexao.Params.Add('Password=masterkey');
  FDConexao.Params.Add('Server=localhost');
  FDConexao.Params.Add('DriverID=FB');

  //
  FDConexao.Open();
{
     FDCallCenter.Close;
     FDCallCenter.Params.Clear;
     FDCallCenter.Params.Database:= 'dbcallcenter.fdb';
     FDCallCenter.Params.UserName:= 'SYSDBA';
     FDCallCenter.Params.Password:= '258159';
     FDCallCenter.Params.DriverID:= 'FB';
     FDCallCenter.Params.AddPair('Server','172.20.120.10');
     FDCallCenter.Open();
}
end;

procedure TdmConexcao.DataModuleDestroy(Sender: TObject);
begin
     FDConexao.Close;
end;

end.
