unit VendaMod;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger, VendaEnt, VendaItemEnt,
  ProdutoEnt;


type

  TVendaDAO = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraVenda(PCodVenda: Integer): TVendaEnt;
//    [MVCListOf(TVendaEnt)] // comando para "trabalhar com listagens
    function ProcuraVendas(PDataVendaIni, PDataVendaFim: TDate): TObjectList<TVendaEnt>;
//    [MVCListOf(TVendaItemEnt)] // comando para "trabalhar com listagens
    function InsereVenda(VendaEnt: TVendaEnt): Integer;
    function fGeraCodVenda: integer;
    function fGeraCodItemVenda(iCodVenda: integer): integer;
    function AlteraVenda(VendaEnt: TVendaEnt): Integer;
    //function ProcuraTodosProd: TObjectList<TProdutoEnt>;
    //function ExcluiProd(PCodProd: Integer): integer;
    //function InsereVenda(VendaEnt: TVendaEnt): Integer;
    //function AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;

  published
    { published declarations }
  end;

implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TVendaMod }

function TVendaDAO.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

// exemplo FOR
{  for MyElem in MyList do
  begin
        MyElem.Prororos
  end;
}


function TVendaDAO.ProcuraVenda(PCodVenda: Integer): TVendaEnt;
var
  ObjVenda: TVendaEnt;
  ObjVendaItem: TVendaItemEnt;
  Query:TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    Query.Open('select * from vendas where codvenda = '+IntToStr(PCodVenda));

    //ObjVenda                := TVendaEnt.Create(TObjectList<TVendaItemEnt>.Create());
    ObjVenda                := TVendaEnt.Create();
    ObjVenda.CodVenda       := Query.FieldByName('codvenda').AsInteger;
    ObjVenda.CodCli         := Query.FieldByName('codcli').AsInteger;
    ObjVenda.DataVenda      := Query.FieldByName('datavenda').AsDateTime;
    ObjVenda.ValorTotVenda  := Query.FieldByName('valortotvenda').AsFloat;
//    ObjVenda.ListaVendaItem := TObjectList<TVendaItemEnt>.Create();
    //
    Query.Open('select * from vendas_itens where codvenda = '+IntToStr(PCodVenda));
    while not Query.Eof do
    begin
      ObjVendaItem               := TVendaItemEnt.Create;
      ObjVendaItem.CodVenda      := Query.FieldByName('codvenda').AsInteger;
      ObjVendaItem.CodItem       := Query.FieldByName('coditem').AsInteger;
      ObjVendaItem.CodProd       := Query.FieldByName('codprod').AsInteger;
      ObjVendaItem.QtdProd       := Query.FieldByName('qtdprod').AsInteger;
      ObjVendaItem.ValorUnitProd := Query.FieldByName('valorunit').AsFloat;
      ObjVendaItem.ValorTotProd  := Query.FieldByName('valortotitem').AsFloat;
      //
      ObjVenda.ListaVendaItem.Add(ObjVendaItem);
      Query.Next;
    end;

    Result := ObjVenda;

  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= ObjVenda;
end;

function TVendaDAO.ProcuraVendas(PDataVendaIni, PDataVendaFim: TDate): TObjectList<TVendaEnt>;
var
  ObjVenda: TVendaEnt;
  ObjVendaItem: TVendaItemEnt;
  Query, Query2: TFDQuery;
  ObjListaVenda: TObjectList<TVendaEnt>;
  oConnection:TFDConnection;
begin
  try
    oConnection       := GetConnection;
    Query             := TFDQuery.Create(nil);
    Query.Connection  := oConnection;
    Query2            := TFDQuery.Create(nil);
    Query2.Connection := oConnection;
    ObjListaVenda     := TObjectList<TVendaEnt>.Create;

    Query.SQL.Text := 'select * from vendas where datavenda between :dataini and :datafim';
    Query.ParamByName('dataini').AsDateTime := PDataVendaIni;
    Query.ParamByName('datafim').AsDateTime := PDataVendaFim;
    Query.Open;
    while not Query.eof do
    begin

      ObjVenda                := TVendaEnt.Create();
      ObjVenda.CodVenda       := Query.FieldByName('codvenda').AsInteger;
      ObjVenda.CodCli         := Query.FieldByName('codcli').AsInteger;
      ObjVenda.DataVenda      := Query.FieldByName('datavenda').AsDateTime;
      ObjVenda.ValorTotVenda  := Query.FieldByName('valortotvenda').AsFloat;
  //    ObjVenda.ListaVendaItem := TObjectList<TVendaItemEnt>.Create();
      //
      Query2.Open('select * from vendas_itens where codvenda = '+IntToStr(ObjVenda.CodVenda));
      while not Query2.Eof do
      begin
        ObjVendaItem               := TVendaItemEnt.Create;
        ObjVendaItem.CodVenda      := Query2.FieldByName('codvenda').AsInteger;
        ObjVendaItem.CodItem       := Query2.FieldByName('coditem').AsInteger;
        ObjVendaItem.CodProd       := Query2.FieldByName('codprod').AsInteger;
        ObjVendaItem.QtdProd       := Query2.FieldByName('qtdprod').AsInteger;
        ObjVendaItem.ValorUnitProd := Query2.FieldByName('valorunit').AsFloat;
        ObjVendaItem.ValorTotProd  := Query2.FieldByName('valortotitem').AsFloat;
        //
        ObjVenda.ListaVendaItem.Add(ObjVendaItem);
        Query2.Next;
      end;

      ObjListaVenda.Add(ObjVenda);

      Query.Next;
    end;


  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= ObjListaVenda;
end;

function TVendaDAO.InsereVenda(VendaEnt: TVendaEnt): Integer;
var
  i, iCodVenda: integer;
  dValorTotVenda: Double;
  Command:TFDCommand;
  Query: TFDQuery;
  oConnection:TFDConnection;
  ObjItemVenda:TVendaItemEnt;
begin
  try
    oConnection         := GetConnection;
    Command             := TFDCommand.Create(nil);
    Command.Connection  := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    try
      oConnection.StartTransaction;
      iCodVenda := fGeraCodVenda;
      Command.Close;
      Command.CommandText.Text:= 'insert into vendas (codvenda, codcli, datavenda) values (:codvenda, :codcli, :datavenda)';
      Command.Params.ParamByName('codvenda').AsInteger := iCodVenda;
      Command.Params.ParamByName('codcli').AsInteger   := VendaEnt.CodCli;
      Command.Params.ParamByName('datavenda').AsDate   := VendaEnt.DataVenda;
      Command.Execute();
      oConnection.Commit;
      //
      dValorTotVenda := 0;

      for ObjItemVenda in VendaEnt.ListaVendaItem do
      begin
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into vendas_itens (codvenda, coditem, codprod, qtdprod, valorunit, valortotitem) '+
                                   ' values (:codvenda, :coditem, :codprod, :qtdprod, :valorunit, :valortotitem)';
        Command.Params.ParamByName('codvenda').AsInteger   := iCodVenda;
        Command.Params.ParamByName('coditem').AsInteger    := fGeraCodItemVenda(iCodVenda);
        Command.Params.ParamByName('codprod').AsInteger    := ObjItemVenda.CodProd;
        Command.Params.ParamByName('qtdprod').AsInteger    := ObjItemVenda.QtdProd;
        Command.Params.ParamByName('valorunit').AsFloat    := ObjItemVenda.ValorUnitProd;
        Command.Params.ParamByName('valortotitem').AsFloat := ObjItemVenda.QtdProd * ObjItemVenda.ValorUnitProd;
        //
        dValorTotVenda := dValorTotVenda + Command.Params.ParamByName('valortotitem').AsFloat;
        //
        Command.Execute();
        oConnection.Commit;
      end;

      oConnection.StartTransaction;
      Command.Close;
      Command.CommandText.Text:= 'update vendas set valortotvenda = :valortotvenda where codvenda = :codvenda';
      Command.Params.ParamByName('valortotvenda').AsFloat := dValorTotVenda;
      Command.Params.ParamByName('codvenda').AsInteger    := iCodVenda;
      Command.Execute();
      oConnection.Commit;
      //
      Result := 200;
    except
      on E:Exception do
      begin
        oConnection.Rollback;
        Log('Error: '+e.Message);
        Result := 500;
      end;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TVendaDAO.fGeraCodVenda: integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open('select gen_id(gen_vendas_id, 1) as SEQUENCIAL from rdb$database');
    Result := Query.FieldByName('sequencial').AsInteger;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TVendaDAO.fGeraCodItemVenda(iCodVenda: integer): integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text := 'select first 1 coditem from vendas_itens where codvenda = :codvenda order by coditem desc';
    Query.ParamByName('codvenda').AsInteger := iCodVenda;
    Query.Open;
    if Query.IsEmpty then
      Result := 1
    else
      Result := Query.FieldByName('coditem').AsInteger+1;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TVendaDAO.AlteraVenda(VendaEnt: TVendaEnt): Integer;
var
  i, iCodVenda: integer;
  dValorTotVenda: Double;
  Command:TFDCommand;
  Query: TFDQuery;
  oConnection:TFDConnection;
  ObjItemVenda:TVendaItemEnt;
begin
  try
    oConnection         := GetConnection;
    Command             := TFDCommand.Create(nil);
    Command.Connection  := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    Query.Open('select * from vendas v where v.codvenda = '+IntToStr(VendaEnt.codvenda));
    if not Query.IsEmpty then
    begin
      try
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' update vendas set codcli = :codcli, datavenda = :datavenda where codvenda = :codvenda ';
        Command.Params.ParamByName('codcli').AsInteger   := VendaEnt.CodCli;
        Command.Params.ParamByName('datavenda').AsDate   := VendaEnt.DataVenda;
        Command.Params.ParamByName('codvenda').AsInteger := VendaEnt.codvenda;
        Command.Execute();
        //
        Command.CommandText.Text:= ' delete from vendas_itens where codvenda = :codvenda ';
        Command.Params.ParamByName('codvenda').AsInteger := VendaEnt.codvenda;
        Command.Execute();
        //
        oConnection.Commit;
        //
        dValorTotVenda := 0;

        for ObjItemVenda in VendaEnt.ListaVendaItem do
        begin
          oConnection.StartTransaction;
          Command.Close;
          Command.CommandText.Text:= ' insert into vendas_itens (codvenda, coditem, codprod, qtdprod, valorunit, valortotitem) '+
                                     ' values (:codvenda, :coditem, :codprod, :qtdprod, :valorunit, :valortotitem)';
          Command.Params.ParamByName('codvenda').AsInteger   := VendaEnt.codvenda;
          Command.Params.ParamByName('coditem').AsInteger    := fGeraCodItemVenda(VendaEnt.codvenda);
          Command.Params.ParamByName('codprod').AsInteger    := ObjItemVenda.CodProd;
          Command.Params.ParamByName('qtdprod').AsInteger    := ObjItemVenda.QtdProd;
          Command.Params.ParamByName('valorunit').AsFloat    := ObjItemVenda.ValorUnitProd;
          Command.Params.ParamByName('valortotitem').AsFloat := ObjItemVenda.QtdProd * ObjItemVenda.ValorUnitProd;
          //
          dValorTotVenda := dValorTotVenda + Command.Params.ParamByName('valortotitem').AsFloat;
          //
          Command.Execute();
          oConnection.Commit;
        end;

        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update vendas set valortotvenda = :valortotvenda where codvenda = :codvenda';
        Command.Params.ParamByName('valortotvenda').AsFloat := dValorTotVenda;
        Command.Params.ParamByName('codvenda').AsInteger    := VendaEnt.codvenda;
        Command.Execute();
        oConnection.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          oConnection.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;




{
function TProdutoDAO.AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produtos where codprod = '+IntToStr(iCodProdAnt));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update produtos set codprod = :codprodnov, descprod = :descprod where codprod = :codprodant';
        Command.Params.ParamByName('codprodnov').AsInteger := ProdutoEnt.CodProd;
        Command.Params.ParamByName('descprod').AsString    := ProdutoEnt.DescProd;
        Command.Params.ParamByName('codprodant').AsInteger := iCodProdAnt; // passo o codprod original caso precise troca-lo
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;
}




(*
function TProdutoDAO.ProcuraTodosProd: TObjectList<TProdutoEnt>;
var
  ProdutoEnt: TProdutoEnt;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  Result := TObjectList<TProdutoEnt>.Create;
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produtos ');
    while not Query.Eof do
    begin
      ProdutoEnt          := TProdutoEnt.Create;
      ProdutoEnt.CodProd  := Query.FieldByName('codprod').Value;
      ProdutoEnt.DescProd := Query.FieldByName('descprod').Value;
      Result.Add(ProdutoEnt);
      Query.Next;
    end;
  finally
  end;
end;

function TProdutoDAO.ExcluiProd(PCodProd: Integer): integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produtos where codprod = '+IntToStr(PCodProd));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from produtos where codprod = :codprod ';
        Command.Params.ParamByName('codprod').AsInteger := PCodProd;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrato na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TVendaDAO.InsereVenda(VendaEnt: TVendaEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    dmConexcao.FDConexao.StartTransaction;
    Command.Close;
    Command.CommandText.Text:= 'insert into vendas (codvenda, codcli, datavenda, valototvenda) values (:codvenda, :codcli, :datavenda, :valototvenda)';
    Command.Params.ParamByName('codprod').AsInteger    := ProdutoEnt.CodProd;
    Command.Params.ParamByName('descprod').AsString    := ProdutoEnt.DescProd;
    Command.Execute();
    dmConexcao.FDConexao.Commit;



    Query.Open(' select * from produtos where codprod = '+IntToStr(ProdutoEnt.CodProd));
    if Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'insert into produtos (codprod, descprod) values (:codprod, :descprod)';
        Command.Params.ParamByName('codprod').AsInteger    := ProdutoEnt.CodProd;
        Command.Params.ParamByName('descprod').AsString    := ProdutoEnt.DescProd;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TProdutoDAO.AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from produtos where codprod = '+IntToStr(iCodProdAnt));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update produtos set codprod = :codprodnov, descprod = :descprod where codprod = :codprodant';
        Command.Params.ParamByName('codprodnov').AsInteger := ProdutoEnt.CodProd;
        Command.Params.ParamByName('descprod').AsString    := ProdutoEnt.DescProd;
        Command.Params.ParamByName('codprodant').AsInteger := iCodProdAnt; // passo o codprod original caso precise troca-lo
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;
*)
end.

