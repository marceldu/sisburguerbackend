unit PedidoEnt;

interface

uses
   System.Generics.Collections, System.SysUtils,
   PedidoProdEnt, PedidoPagtoEnt, PedidoProdAcaoEnt,
   MVCFramework.Serializer.Commons; // sempre incluir essa classe em caso de listagem
type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TPedidoEnt = class
  private
    FCodPed: Integer;
    FCodPes: Integer;
    FNomePes: String;
    FValorPed: Double;
    FViagem: Smallint;
    FPago: Smallint;
    FDataHora: TDateTime;
    FStatus: Integer;
    FObs: String;
    FListaPedidoProd: TObjectList<TPedidoProdEnt>;
    FListaPedidoPagto: TObjectList<TPedidoPagtoEnt>;
    FListaPedidoProdAcao: TObjectList<TPedidoProdAcaoEnt>;
  public
    constructor Create(ListaPedidoProd: TObjectList<TPedidoProdEnt>;
                       ListaPedidoPagto: TObjectList<TPedidoPagtoEnt>;
                       ListaPedidoProdAcao: TObjectList<TPedidoProdAcaoEnt>)  overload;
    constructor Create()  overload;
    destructor Destroy; override;

    // todas as propriedades abaixo de preferencia minusculas (exemplo: "codvenda")
    property codped: Integer read FCodPed write FCodPed;
    property codpes: Integer read FCodPes write FCodPes;
    property nomepes: String read FNomePes write FNomePes;
    property valorped: Double read FValorPed write FValorPed;
    property viagem: SmallInt read FViagem write FViagem;
    property pago: SmallInt read FPago write FPago;
    property datahora: TDateTime read FDataHora write FDataHora;
    property status: Integer read FStatus write FStatus;
    property obs: String read FObs write FObs;
    [MVCListOf(TPedidoProdEnt)] // comando para "trabalhar com listagens
    property listapedidoprod: TObjectList<TPedidoProdEnt> read FListaPedidoProd
                                                  write FListaPedidoProd;
    [MVCListOf(TPedidoPagtoEnt)] // comando para "trabalhar com listagens
    property listapedidopagto: TObjectList<TPedidoPagtoEnt> read FListaPedidoPagto
                                                  write FListaPedidoPagto;
    [MVCListOf(TPedidoProdAcaoEnt)] // comando para "trabalhar com listagens
    property listapedidoprodacao: TObjectList<TPedidoProdAcaoEnt> read FListaPedidoProdAcao
                                                  write FListaPedidoProdAcao;


  end;

implementation

{ TVendaEnt }

constructor TPedidoEnt.Create(ListaPedidoProd: TObjectList<TPedidoProdEnt>;
    ListaPedidoPagto: TObjectList<TPedidoPagtoEnt>;
    ListaPedidoProdAcao: TObjectList<TPedidoProdAcaoEnt>);
begin
  FListaPedidoProd := ListaPedidoProd;
  FListaPedidoPagto := ListaPedidoPagto;
  FListaPedidoProdAcao := ListaPedidoProdAcao;
end;

constructor TPedidoEnt.Create;
begin
  FListaPedidoProd := TObjectList<TPedidoProdEnt>.Create();
  FListaPedidoPagto := TObjectList<TPedidoPagtoEnt>.Create();
  FListaPedidoProdAcao := TObjectList<TPedidoProdAcaoEnt>.Create();
end;

destructor TPedidoEnt.Destroy;
begin
//  if Assigned(FListaVendaItem) then
//    FListaVendaItem.Free;
  inherited;
end;

{ TVendaEnt }



end.
