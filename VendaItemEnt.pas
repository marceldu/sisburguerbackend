unit VendaItemEnt;

interface

type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TVendaItemEnt = class
  private
    FCodVenda: Integer;
    FCodItem: Integer;
    FCodProd: Integer;
    FQtdProd: Integer;
    FValorUnitProd: Double;
    FValorTotProd: Double;
  public
    // todas as propriedades abaixo de preferencia minusculas (exemplo: "codvenda")
    property codvenda: Integer read FCodVenda write FCodVenda;
    property coditem: Integer read FCodItem write FCodItem;
    property codprod: Integer read FCodProd write FCodProd;
    property qtdprod: Integer read FQtdProd write FQtdProd;
    property valorunitprod: Double read FValorUnitProd write FValorUnitProd;
    property valortotprod: Double read FValorTotProd write FValorTotProd;
  end;

implementation

end.
