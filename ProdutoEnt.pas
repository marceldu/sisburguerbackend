unit ProdutoEnt;

interface

type
  [MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TProdutoEnt = class
  private
    FCodProd: Integer;
    FDescProd: string;
    FValUnit: Double;
    FCodTipoProd: Integer;
  public
    // propriedade sempre minuscula (codprod)
    property codprod: Integer read FCodProd write FCodProd;
    property descprod: string read FDescProd write FDescProd;
    property valunit: Double read FValUnit write FValUnit;
    property codtipoprod: Integer read FCodTipoProd write FCodTipoProd;
  end;

implementation

end.
