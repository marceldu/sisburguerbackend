unit model_produtos;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger;


type

  TProduto = class
  private
    { private declarations }
    Fcodprod:Integer;
    Fdescprod:String;
  protected
    { protected declarations }
     class function GetConnection:TFDConnection;

  public
    { public declarations }
     property codprod:Integer read Fcodprod write Fcodprod;
     property descprod:String read Fdescprod write Fdescprod;
     class function GetAll():TObjectList<TProduto>;
     class function Get(value:integer):TProduto;
     class function insereprod(Produto:TProduto;jSon:String):Integer;

  published
    { published declarations }
  end;

implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TCrmMalta }

class function TProduto.Get(value: integer): TProduto;
var
  Obj:TProduto;
  Query:TFDQuery;
  oConnection:TFDConnection;
begin
     try
         oConnection:= GetConnection;
         Query:= TFDQuery.Create(nil);
         Query.Connection:= oConnection;

         Query.Open('select * from produtos where codprod='+IntToStr(value));

          Obj:= TProduto.Create;
          Obj.codprod  := Query.FieldByName('codprod').AsInteger;
          Obj.descprod := Query.FieldByName('descprod').AsString;

      finally
            Query.Free;
            oConnection.Close;
            FreeAndNil(oConnection);
      end;
      Result:= Obj;
end;

class function TProduto.GetAll: TObjectList<TProduto>;
var
  oList:TObjectList<TProduto>;
  Query:TFDQuery;
  Obj:TProduto;
  oConnection:TFDConnection;
begin
      try
         oConnection:= GetConnection;
         oList:= TObjectList<TProduto>.Create();


         Query:= TFDQuery.Create(nil);
         Query.Connection:= oConnection;

         Query.Open('select * from produtos');

         while not Query.Eof do
         begin
              Obj:= TProduto.Create;
              Obj.codprod  := Query.FieldByName('codprod').AsInteger;
              Obj.descprod := Query.FieldByName('descprod').AsString;
              oList.Add(Obj);
              Query.Next;
         end;
      finally
            Query.Free;
            oConnection.Close;
            FreeAndNil(oConnection);
      end;
      Result:= oList;
end;

class function TProduto.insereprod(Produto: TProduto;jSon:String): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
      try
         oConnection:= GetConnection;

         Command:= TFDCommand.Create(nil);
         Command.Connection:= oConnection;

         Query:= TFDQuery.Create(nil);
         Query.Connection:= oConnection;
         Query.Open(' select * from produtos  '+
                           ' where codprod = '+IntToStr(Produto.Fcodprod));
         if Query.IsEmpty then
         begin
             try
                 dmConexcao.FDConexao.StartTransaction;
                 Command.Close;
                 Command.CommandText.Text:= 'insert into produtos (codprod, descprod) values (:codprod, :descprod)';
                 Command.Params.ParamByName('codprod').AsInteger    := Produto.Fcodprod;
                 Command.Params.ParamByName('descprod').AsString    := Produto.Fdescprod;
                 Command.Execute();
                 dmConexcao.FDConexao.Commit;

                 Result:= 200;
             except
                   on E:Exception do
                   begin
                         dmConexcao.FDConexao.Rollback;
                         Log('Error: '+e.Message+' ['+DeleteLineBreaks(jSon)+']');
                         Result:= 500;
                   end;
             end;
         end
         else
         begin
              Log('Registro n�o encontrato na credor_malta_discador, ['+jSon+']');
              Result:= 200;
         end;
      finally
            Command.Free;
            Query.Free;
      end;
end;
{
class function TProduto.alteraprod(Produto: TProduto;jSon:String): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
      try
         oConnection:= GetConnection;

         Command:= TFDCommand.Create(nil);
         Command.Connection:= oConnection;

         Query:= TFDQuery.Create(nil);
         Query.Connection:= oConnection;
         Query.Open(' select * from produtos  '+
                           ' where codprod = '+IntToStr(Produto.codprod));
         if not Query.IsEmpty then
         begin
             try
                 dmConexcao.FDConexao.StartTransaction;
                 Command.Close;
                 Command.CommandText.Text:= 'update produtos descprod = :descprod where codprod = :codprod';
                 Command.Params.ParamByName('codprod').AsInteger := Produto.codprod;
                 Command.Params.ParamByName('descprod').AsString := Produto.descprod;
                 Command.Execute();
                 dmConexcao.FDConexao.Commit;

                 Result:= 200;
             except
                   on E:Exception do
                   begin
                         dmConexcao.FDConexao.Rollback;
                         Log('Error: '+e.Message+' ['+DeleteLineBreaks(jSon)+']');
                         Result:= 500;
                   end;
             end;
         end
         else
         begin
              Log('Registro n�o encontrato na credor_malta_discador, ['+jSon+']');
              Result:= 200;
         end;
      finally
            Command.Free;
            Query.Free;
      end;
end;
}
class function TProduto.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

end.

