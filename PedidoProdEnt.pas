unit PedidoProdEnt;

interface

uses System.Generics.Collections, System.SysUtils, PedidoProdIngEnt,
  PedidoProdAcaoEnt,
  MVCFramework.Serializer.Commons; // sempre incluir essa classe em caso de listagem
type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TPedidoProdEnt = class
  private
    FCodPed: Integer;
    FCodSeqProd: Integer;
    FCodProd: Integer;
    FDescProd: String;
    FValorUnitProd: Double;
    FQtdProd: Integer;
    FStatus: Integer;
    FValorTotProd: Double;
    FValorAcres: Double;
    FListaPedidoProdIng: TObjectList<TPedidoProdIngEnt>;
    FListaPedidoProdAcao: TObjectList<TPedidoProdAcaoEnt>;
  public
    constructor Create(ListaPedidoProdIng: TObjectList<TPedidoProdIngEnt>)  overload;
    constructor Create()  overload;
    destructor Destroy; override;

    // todas as propriedades abaixo de preferencia minusculas
    property codped: Integer read FCodPed write FCodPed;
    property codseqprod: Integer read FCodSeqProd write FCodSeqProd;
    property codprod: Integer read FCodProd write FCodProd;
    property descprod: String read FDescProd write FDescProd;
    property valorunitprod: Double read FValorUnitProd write FValorUnitProd;
    property qtdprod: Integer read FQtdProd write FQtdProd;
    property status: Integer read FStatus write FStatus;
    property valortotprod: Double read FValorTotProd write FValorTotProd;
    property valoracres: Double read FValorAcres write FValorAcres;
    [MVCListOf(TPedidoProdIngEnt)] // comando para "trabalhar com listagens
    property listapedidoproding: TObjectList<TPedidoProdIngEnt>
          read FListaPedidoProdIng write FListaPedidoProdIng;
    [MVCListOf(TPedidoProdAcaoEnt)] // comando para "trabalhar com listagens
    property listapedidoprodacao: TObjectList<TPedidoProdAcaoEnt>
          read FListaPedidoProdAcao write FListaPedidoProdAcao;
  end;

implementation

constructor TPedidoProdEnt.Create(ListaPedidoProdIng: TObjectList<TPedidoProdIngEnt>);
begin
  FListaPedidoProdIng  := ListaPedidoProdIng;
  FListaPedidoProdAcao := ListaPedidoProdAcao;
end;

constructor TPedidoProdEnt.Create;
begin
  FListaPedidoProdIng := TObjectList<TPedidoProdIngEnt>.Create();
  FListaPedidoProdAcao := TObjectList<TPedidoProdAcaoEnt>.Create();
end;

destructor TPedidoProdEnt.Destroy;
begin
//  if Assigned(FListaVendaItem) then
//    FListaVendaItem.Free;
  inherited;
end;

end.
