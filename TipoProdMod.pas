unit TipoProdMod;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger, TipoProdEnt;


type
  TTipoProdDAO = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraTipoProdID(PCodTipoProd: Integer): TTipoProdEnt;
    function ProcuraTodosTipoProd: TObjectList<TTipoProdEnt>;
    function ExcluiTipoProd(PCodTipoProd: Integer): integer;
    function InsereTipoProd(TipoProdEnt: TTipoProdEnt): Integer;
    function AlteraTipoProd(iCodTipoProdAnt: integer; TipoProdEnt: TTipoProdEnt): Integer;

  published
    { published declarations }
  end;


implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TTipoProdMod }

function TTipoProdDAO.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

function TTipoProdDAO.ProcuraTipoProdID(PCodTipoProd: Integer): TTipoProdEnt;
var
  Obj:TTipoProdEnt;
  Query:TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;

    Query.Open('select * from tipoprod where codtipoprod = '+IntToStr(PCodTipoProd));

    Obj          := TTipoProdEnt.Create;
    Obj.codtipoprod  := Query.FieldByName('codtipoprod').AsInteger;
    Obj.desctipoprod := Query.FieldByName('desctipoprod').AsString;
  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= Obj;
end;

function TTipoProdDAO.ProcuraTodosTipoProd: TObjectList<TTipoProdEnt>;
var
  TipoProdEnt: TTipoProdEnt;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  Result := TObjectList<TTipoProdEnt>.Create;
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from tipoprod ');
    while not Query.Eof do
    begin
      TipoProdEnt          := TTipoProdEnt.Create;
      TipoProdEnt.CodTipoProd  := Query.FieldByName('codtipoprod').Value;
      TipoProdEnt.DescTipoProd := Query.FieldByName('desctipoprod').Value;
      Result.Add(TipoProdEnt);
      Query.Next;
    end;
  finally
  end;
end;

function TTipoProdDAO.ExcluiTipoProd(PCodTipoProd: Integer): integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from tipoprod where codtipoprod = '+IntToStr(PCodTipoProd));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from tipoprod where codtipoprod = :codtipoprod ';
        Command.Params.ParamByName('codtipoprod').AsInteger := PCodTipoProd;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrato na TIPOPROD');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TTipoProdDAO.InsereTipoProd(TipoProdEnt: TTipoProdEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from tipoprod where codtipoprod = '+IntToStr(TipoProdEnt.codtipoprod));
    if Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'insert into tipoprod (codtipoprod, desctipoprod) values (:codtipoprod, :desctipoprod)';
        Command.Params.ParamByName('codtipoprod').AsInteger    := TipoProdEnt.CodTipoProd;
        Command.Params.ParamByName('desctipoprod').AsString    := TipoProdEnt.DescTipoProd;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na TIPOPROD');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TTipoProdDAO.AlteraTipoProd(iCodTipoProdAnt: integer; TipoProdEnt: TTipoProdEnt): Integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from tipoprod where codtipoprod = '+IntToStr(iCodTipoProdAnt));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= 'update tipoprod set codtipoprod = :codtipoprodnov, desctipoprod = :desctipoprod where codtipoprod = :codtipoprodant';
        Command.Params.ParamByName('codtipoprodnov').AsInteger := TipoProdEnt.CodTipoProd;
        Command.Params.ParamByName('desctipoprod').AsString    := TipoProdEnt.DescTipoProd;
        Command.Params.ParamByName('codtipoprodant').AsInteger := iCodTipoProdAnt; // passo o codtipoprod original caso precise troca-lo
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrado na TIPOPROD');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

end.

