unit PedidoPagtoEnt;

interface

type
  //[MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TPedidoPagtoEnt = class
  private
    FCodPed: Integer;
    FCodSeqPagto: Integer;
    FCodFormPagto: Integer;
    FDescFormPagto: String;
    FValorPagto: Double;
  public
    // todas as propriedades abaixo de preferencia minusculas (exemplo: "codvenda")
    property codped: Integer read FCodPed write FCodPed;
    property codseqpagto: Integer read FCodSeqPagto write FCodSeqPagto;
    property codformpagto: Integer read FCodFormPagto write FCodFormPagto;
    property descformpagto: String read FDescFormPagto write FDescFormPagto;
    property valorpagto: Double read FValorPagto write FValorPagto;
  end;

implementation

end.
