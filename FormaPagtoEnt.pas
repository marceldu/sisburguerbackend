unit FormaPagtoEnt;

interface

type
  [MapperJSONNaming(JSONNameLowerCase)] // tudo minusculo

  TFormaPagtoEnt = class
  private
    FCodFormPagto: Integer;
    FDescFormPagto: string;
  public
    // propriedade sempre minuscula (codprod)
    property codformpagto: Integer read FCodFormPagto write FCodFormPagto;
    property descformpagto: string read FDescFormPagto write FDescFormPagto;
  end;

implementation

end.
