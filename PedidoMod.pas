unit PedidoMod;

interface

uses System.Generics.Collections,System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, uDM, MVCFramework.Logger, PedidoEnt, PedidoProdEnt,
  ProdutoEnt, PedidoProdIngEnt, PedidoPagtoEnt, PedidoProdAcaoEnt;


type

  TPedidoDAO = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraPedido(PCodPed: Integer): TPedidoEnt;
//    [MVCListOf(TPedidoProdEnt)] // comando para "trabalhar com listagens
//    [MVCListOf(TPedidoProdIngEnt)] // comando para "trabalhar com listagens
//    [MVCListOf(TPedidoPagtoEnt)] // comando para "trabalhar com listagens
    function ProcuraPedidos(PDataIni, PDataFim: TDate): TObjectList<TPedidoEnt>;
    function InserePedido(PedidoEnt: TPedidoEnt): Integer;
    function fGeraCodPed: integer;
    function fGeraCodSeqProd(iCodPed: integer): integer;
    function fGeraCodSeqPagto(iCodPed: integer): integer;
    function fGeraCodSeqIng(iCodPed, iCodSeqProd: integer): integer;
    function AlteraPedido(PedidoEnt: TPedidoEnt): Integer;
    function ExcluiPedido(PCodPed: Integer): integer;
{
    [MVCListOf(TPedidoEnt)] // comando para "trabalhar com listagens
    function ProcuraPedidos(PDataVendaIni, PDataVendaFim: TDate): TObjectList<TPedidoEnt>;
    [MVCListOf(TPedidoPedEnt)] // comando para "trabalhar com listagens
    function InserePedido(PedidoEnt: TPedidoEnt): Integer;
    function fGeraCodPed: integer;
    function fGeraCodSeqProd(iCodVenda: integer): integer;
    function fGeraCodSeqIng(iCodVenda, iCodSeqProd: integer): integer;
    function AlteraPedido(PedidoEnt: TPedidoEnt): Integer;
}
    //function ProcuraTodosProd: TObjectList<TProdutoEnt>;
    //function ExcluiProd(PCodProd: Integer): integer;
    //function InsereVenda(VendaEnt: TVendaEnt): Integer;
    //function AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;

  published
    { published declarations }
  end;

implementation

function DeleteLineBreaks(const S: string): string;
var
   Source, SourceEnd: PChar;
begin
   Source := Pointer(S) ;
   SourceEnd := Source + Length(S) ;
   while Source < SourceEnd do
   begin
     case Source^ of
       #9: Source^ := #32;
       #10: Source^ := #32;
       #13: Source^ := #32;
     end;
     Inc(Source) ;
   end;
   Result := S;
End;


{ TPedidoMod }

function TPedidoDAO.GetConnection: TFDConnection;
begin
    dmConexcao:= TdmConexcao.Create(nil);
    Result:= dmConexcao.FDconexao;
end;

// exemplo FOR
{  for MyElem in MyList do
  begin
        MyElem.Prororos
  end;
}

function TPedidoDAO.ProcuraPedido(PCodPed: Integer): TPedidoEnt;
var
  ObjPedido: TPedidoEnt;
  ObjPedidoProd: TPedidoProdEnt;
  ObjPedidoProdIng: TPedidoProdIngEnt;
  ObjPedidoPagto: TPedidoPagtoEnt;
  Query, Query2 :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection       := GetConnection;
    Query             := TFDQuery.Create(nil);
    Query2            := TFDQuery.Create(nil);
    Query.Connection  := oConnection;
    Query2.Connection := oConnection;
    Query.SQL.Text    := ' select pe.*, pa.nomepes '+
                         ' from pedido pe '+
                         ' left join pessoa pa on pa.codpes = pe.codpes '+
                         ' where pe.codped = '+IntToStr(PCodPed);
    Query.Open;
    //ObjPedido                := TPedidoEnt.Create(TObjectList<TVendaItemEnt>.Create());
    ObjPedido          := TPedidoEnt.Create();
    ObjPedido.CodPed   := Query.FieldByName('codped').AsInteger;
    ObjPedido.CodPes   := Query.FieldByName('codpes').AsInteger;
    ObjPedido.NomePes  := Query.FieldByName('nomepes').AsString;
    ObjPedido.ValorPed := Query.FieldByName('valorped').AsFloat;
    ObjPedido.Viagem   := Query.FieldByName('viagem').AsInteger;
    ObjPedido.Pago     := Query.FieldByName('pago').AsInteger;
    ObjPedido.datahora := Query.FieldByName('datahora').AsDateTime;
    ObjPedido.Status   := Query.FieldByName('status').AsInteger;
    ObjPedido.Obs      := Query.FieldByName('obs').AsString;

//    ObjPedido.ListaPedidoProd := TObjectList<TPedidoProdEnt>.Create();
    //
    Query.SQL.Text    := ' select pp.*, p.descprod from pedidoprod pp '+
                         ' inner join produto p on p.codprod = pp.codprod '+
                         ' where pp.codped = '+IntToStr(PCodPed);
    Query.open;
    while not Query.Eof do
    begin
      ObjPedidoProd               := TPedidoProdEnt.Create;
      ObjPedidoProd.CodPed        := Query.FieldByName('codped').AsInteger;
      ObjPedidoProd.CodSeqProd    := Query.FieldByName('codseqprod').AsInteger;
      ObjPedidoProd.CodProd       := Query.FieldByName('codprod').AsInteger;
      ObjPedidoProd.DescProd      := Query.FieldByName('descprod').AsString;
      ObjPedidoProd.QtdProd       := Query.FieldByName('qtdprod').AsInteger;
      ObjPedidoProd.ValorUnitProd := Query.FieldByName('valorunitprod').AsFloat;
      ObjPedidoProd.ValorTotProd  := Query.FieldByName('valortotprod').AsFloat;
      ObjPedidoProd.ValorAcres    := Query.FieldByName('valoracres').AsFloat;
      //
      Query2.SQL.Text := ' select ppi.*, p.descprod from pedidoproding ppi '+
                         ' inner join produto p on p.codprod = ppi.codproding '+
                         ' where ppi.codped = '+IntToStr(PCodPed)+
                         '    and ppi.codseqprod = '+Query.FieldByName('codseqprod').AsString;
      Query2.Open;
      while not Query2.Eof do
      begin
        ObjPedidoProdIng              := TPedidoProdIngEnt.Create;
        ObjPedidoProdIng.codped       := Query2.FieldByName('codped').AsInteger;
        ObjPedidoProdIng.codseqprod   := Query2.FieldByName('codseqprod').AsInteger;
        ObjPedidoProdIng.codseqing    := Query2.FieldByName('codseqing').AsInteger;
        ObjPedidoProdIng.codproding   := Query2.FieldByName('codproding').AsInteger;
        ObjPedidoProdIng.descproding  := Query2.FieldByName('descprod').AsString;
        ObjPedidoProdIng.valoruniting := Query2.FieldByName('valoruniting').AsFloat;
        ObjPedidoProdIng.qtdproding   := Query2.FieldByName('qtdproding').AsInteger;
        ObjPedidoProdIng.ingpadrao    := Query2.FieldByName('ingpadrao').AsInteger;

        ObjPedidoProd.listapedidoproding.Add(ObjPedidoProdIng);
        Query2.Next;
      end;
      //
      ObjPedido.listapedidoprod.Add(ObjPedidoProd);
      Query.Next;
    end;


    //
    Query.SQL.Text    := ' select pp.*, fp.descformpagto from pedidopagto pp '+
                         ' inner join formapagto fp on fp.codformpagto = pp.codformpagto '+
                         ' where pp.codped = '+IntToStr(PCodPed);
    Query.open;
    while not Query.Eof do
    begin
      ObjPedidoPagto               := TPedidoPagtoEnt.Create;
      ObjPedidoPagto.CodPed        := Query.FieldByName('codped').AsInteger;
      ObjPedidoPagto.CodSeqPagto   := Query.FieldByName('codseqpagto').AsInteger;
      ObjPedidoPagto.codformpagto  := Query.FieldByName('codformpagto').AsInteger;
      ObjPedidoPagto.descformpagto := Query.FieldByName('descformpagto').AsString;
      ObjPedidoPagto.valorpagto    := Query.FieldByName('valorpagto').AsFloat;
      //
      ObjPedido.listapedidopagto.Add(ObjPedidoPagto);
      Query.Next;
    end;


    Result := ObjPedido;

  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= ObjPedido;
end;

function TPedidoDAO.ProcuraPedidos(PDataIni, PDataFim: TDate): TObjectList<TPedidoEnt>;
var
  ObjPedido: TPedidoEnt;
  ObjPedidoProd: TPedidoProdEnt;
  ObjPedidoProdIng: TPedidoProdIngEnt;
  ObjPedidoPagto: TPedidoPagtoEnt;
  ObjListaPedido: TObjectList<TPedidoEnt>;
  Query, Query2, Query3 :TFDQuery;
  oConnection:TFDConnection;
  PCodPed: integer;
begin
  try
    oConnection       := GetConnection;
    Query             := TFDQuery.Create(nil);
    Query2            := TFDQuery.Create(nil);
    Query3            := TFDQuery.Create(nil);
    Query.Connection  := oConnection;
    Query2.Connection := oConnection;
    Query3.Connection := oConnection;
    ObjListaPedido     := TObjectList<TPedidoEnt>.Create;

    Query3.SQL.Text := 'select codped from pedido where cast(datahora as date) between :dataini and :datafim';
    Query3.ParamByName('dataini').AsDateTime := PDataIni;
    Query3.ParamByName('datafim').AsDateTime := PDataFim;
    Query3.Open;

    while not Query3.eof do
    begin
      PCodPed := Query3.FieldByName('codped').AsInteger;
      //
    Query.SQL.Text    := ' select pe.*, pa.nomepes '+
                         ' from pedido pe '+
                         ' left join pessoa pa on pa.codpes = pe.codpes '+
                         ' where pe.codped = '+IntToStr(PCodPed);
      Query.Open;
      //ObjPedido                := TPedidoEnt.Create(TObjectList<TVendaItemEnt>.Create());
      ObjPedido          := TPedidoEnt.Create();
      ObjPedido.CodPed   := Query.FieldByName('codped').AsInteger;
      ObjPedido.CodPes   := Query.FieldByName('codpes').AsInteger;
      ObjPedido.NomePes  := Query.FieldByName('nomepes').AsString;
      ObjPedido.ValorPed := Query.FieldByName('valorped').AsFloat;
      ObjPedido.Viagem   := Query.FieldByName('viagem').AsInteger;
      ObjPedido.Pago     := Query.FieldByName('pago').AsInteger;
      ObjPedido.datahora := Query.FieldByName('datahora').AsDateTime;
      ObjPedido.status   := Query.FieldByName('status').AsInteger;
      ObjPedido.Obs      := Query.FieldByName('obs').AsString;

  //    ObjPedido.ListaPedidoProd := TObjectList<TPedidoProdEnt>.Create();
      //
      Query.SQL.Text    := ' select pp.*, p.descprod from pedidoprod pp '+
                           ' inner join produto p on p.codprod = pp.codprod '+
                           ' where pp.codped = '+IntToStr(PCodPed);
      Query.open;
      while not Query.Eof do
      begin
        ObjPedidoProd               := TPedidoProdEnt.Create;
        ObjPedidoProd.CodPed        := Query.FieldByName('codped').AsInteger;
        ObjPedidoProd.CodSeqProd    := Query.FieldByName('codseqprod').AsInteger;
        ObjPedidoProd.CodProd       := Query.FieldByName('codprod').AsInteger;
        ObjPedidoProd.DescProd      := Query.FieldByName('descprod').AsString;
        ObjPedidoProd.QtdProd       := Query.FieldByName('qtdprod').AsInteger;
        ObjPedidoProd.Status        := Query.FieldByName('status').AsInteger;
        ObjPedidoProd.ValorUnitProd := Query.FieldByName('valorunitprod').AsFloat;
        ObjPedidoProd.ValorTotProd  := Query.FieldByName('valortotprod').AsFloat;
        ObjPedidoProd.ValorAcres    := Query.FieldByName('valoracres').AsFloat;
        //
        Query2.SQL.Text := ' select ppi.*, p.descprod from pedidoproding ppi '+
                           ' inner join produto p on p.codprod = ppi.codproding '+
                           ' where ppi.codped = '+IntToStr(PCodPed)+
                           '    and ppi.codseqprod = '+Query.FieldByName('codseqprod').AsString;
        Query2.Open;
        while not Query2.Eof do
        begin
          ObjPedidoProdIng              := TPedidoProdIngEnt.Create;
          ObjPedidoProdIng.codped       := Query2.FieldByName('codped').AsInteger;
          ObjPedidoProdIng.codseqprod   := Query2.FieldByName('codseqprod').AsInteger;
          ObjPedidoProdIng.codseqing    := Query2.FieldByName('codseqing').AsInteger;
          ObjPedidoProdIng.codproding   := Query2.FieldByName('codproding').AsInteger;
          ObjPedidoProdIng.descproding  := Query2.FieldByName('descprod').AsString;
          ObjPedidoProdIng.valoruniting := Query2.FieldByName('valoruniting').AsFloat;
          ObjPedidoProdIng.qtdproding   := Query2.FieldByName('qtdproding').AsInteger;
          ObjPedidoProdIng.ingpadrao    := Query2.FieldByName('ingpadrao').AsInteger;

          ObjPedidoProd.listapedidoproding.Add(ObjPedidoProdIng);
          Query2.Next;
        end;
        //
        ObjPedido.listapedidoprod.Add(ObjPedidoProd);
        Query.Next;
      end;


      //
      Query.SQL.Text    := ' select pp.*, fp.descformpagto from pedidopagto pp '+
                           ' inner join formapagto fp on fp.codformpagto = pp.codformpagto '+
                           ' where pp.codped = '+IntToStr(PCodPed);
      Query.open;
      while not Query.Eof do
      begin
        ObjPedidoPagto               := TPedidoPagtoEnt.Create;
        ObjPedidoPagto.CodPed        := Query.FieldByName('codped').AsInteger;
        ObjPedidoPagto.CodSeqPagto   := Query.FieldByName('codseqpagto').AsInteger;
        ObjPedidoPagto.codformpagto  := Query.FieldByName('codformpagto').AsInteger;
        ObjPedidoPagto.descformpagto := Query.FieldByName('descformpagto').AsString;
        ObjPedidoPagto.valorpagto    := Query.FieldByName('valorpagto').AsFloat;
        //
        ObjPedido.listapedidopagto.Add(ObjPedidoPagto);
        Query.Next;
      end;
      //
      ObjListaPedido.Add(ObjPedido);
      //
      Query3.Next;
    end;


    Result := ObjListaPedido;

  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;

end;

function TPedidoDAO.InserePedido(PedidoEnt: TPedidoEnt): Integer;
var
  i, iCodPed, iCodSeqProd, iCodSeqIng: integer;
  dValorAcres, dValorTotPed, dValorProd: Double;
  Command:TFDCommand;
  Query: TFDQuery;
  oConnection:TFDConnection;
  ObjPedidoProd:TPedidoProdEnt;
  ObjPedidoProdIng:TPedidoProdIngEnt;
  ObjPedidoProdAcao:TPedidoProdAcaoEnt;
  ObjPedidoPagto:TPedidoPagtoEnt;
begin
  try
    oConnection         := GetConnection;
    Command             := TFDCommand.Create(nil);
    Command.Connection  := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    try
      oConnection.StartTransaction;
      iCodPed := fGeraCodPed;
      Command.Close;
      Command.CommandText.Text:= ' insert into pedido (codped, codpes, valorped, viagem, pago, datahora, status, obs) '+
                                 ' values (:codped, :codpes, :valorped, :viagem, :pago, :datahora, :status, :obs)';
      Command.Params.ParamByName('codped').AsInteger    := iCodPed;
      Command.Params.ParamByName('codpes').AsInteger    := PedidoEnt.codpes;
      Command.Params.ParamByName('valorped').AsFloat    := PedidoEnt.valorped;
      Command.Params.ParamByName('viagem').AsInteger    := PedidoEnt.viagem;
      Command.Params.ParamByName('pago').AsInteger      := PedidoEnt.pago;
      Command.Params.ParamByName('datahora').AsDateTime := Now;
      Command.Params.ParamByName('status').AsInteger    := PedidoEnt.status;
      Command.Params.ParamByName('obs').AsString        := PedidoEnt.obs;
      Command.Execute();
      oConnection.Commit;
      //
      dValorTotPed := 0;

      for ObjPedidoProd in PedidoEnt.listapedidoprod do
      begin
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into pedidoprod (codped, codseqprod, codprod, valorunitprod, qtdprod, status, valortotprod) '+
                                   ' values (:codped, :codseqprod, :codprod, :valorunitprod, :qtdprod, :status, :valortotprod)';
        Command.Params.ParamByName('codped').AsInteger      := iCodPed;
        iCodSeqProd                                         := fGeraCodSeqProd(iCodPed);
        Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
        Command.Params.ParamByName('codprod').AsInteger     := ObjPedidoProd.codprod;
        Command.Params.ParamByName('valorunitprod').AsFloat := ObjPedidoProd.valorunitprod;
        Command.Params.ParamByName('qtdprod').AsInteger     := ObjPedidoProd.qtdprod;
        Command.Params.ParamByName('status').AsInteger      := ObjPedidoProd.status;
        Command.Params.ParamByName('valortotprod').AsFloat := ObjPedidoProd.valorunitprod;
        //
        dValorProd := (ObjPedidoProd.valorunitprod * ObjPedidoProd.qtdprod);
        //
        Command.Execute();
        oConnection.Commit;

        for ObjPedidoProdAcao in ObjPedidoProd.listapedidoprodacao do
        begin
          oConnection.StartTransaction;
          Command.Close;
          Command.CommandText.Text:= ' insert into pedidoprodacao (codped, codseqprod, codacao) '+
                                     ' values (:codped, :codseqprod, :codacao)';
          Command.Params.ParamByName('codped').AsInteger      := iCodPed;
          Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
          Command.Params.ParamByName('codacao').AsInteger     := ObjPedidoProdAcao.codacao;
          //
          Command.Execute();
          oConnection.Commit;
        end;

        dValorAcres := 0;
        for ObjPedidoProdIng in ObjPedidoProd.listapedidoproding do
        begin
          oConnection.StartTransaction;
          Command.Close;
          Command.CommandText.Text:= ' insert into pedidoproding (codped, codseqprod, codseqing, codproding, valoruniting, qtdproding, ingpadrao) '+
                                     ' values (:codped, :codseqprod, :codseqing, :codproding, :valoruniting, :qtdproding, :ingpadrao)';
          Command.Params.ParamByName('codped').AsInteger      := iCodPed;
          Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
          iCodSeqIng                                          := fGeraCodSeqIng(iCodPed,iCodSeqProd);
          Command.Params.ParamByName('codseqing').AsInteger   := iCodSeqIng;
          Command.Params.ParamByName('codproding').AsInteger  := ObjPedidoProdIng.codproding;
          Command.Params.ParamByName('valoruniting').AsFloat  := ObjPedidoProdIng.valoruniting;
          Command.Params.ParamByName('qtdproding').AsInteger  := ObjPedidoProdIng.qtdproding;
          Command.Params.ParamByName('ingpadrao').AsInteger   := ObjPedidoProdIng.ingpadrao;
          //
          Command.Execute();
          oConnection.Commit;
          //
          if ObjPedidoProdIng.valoruniting > 0 then
            dValorAcres := dValorAcres + (ObjPedidoProdIng.valoruniting * ObjPedidoProdIng.qtdproding);
        end;
        //
        dValorTotPed := dValorTotPed + dValorProd + (dValorAcres * ObjPedidoProd.qtdprod);
        //
        // atualiza valor total do produto
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' update pedidoprod set valortotprod = :valortotprod, valoracres = :valoracres '+
                                   ' where codped = :codped and codseqprod = :codseqprod';
        Command.Params.ParamByName('codped').AsInteger     := iCodPed;
        Command.Params.ParamByName('codseqprod').AsInteger := iCodSeqProd;
        Command.Params.ParamByName('valortotprod').AsFloat := dValorProd + (dValorAcres * ObjPedidoProd.qtdprod);
        Command.Params.ParamByName('valoracres').AsFloat   := dValorAcres * ObjPedidoProd.qtdprod;
        Command.Execute();
        oConnection.Commit;
      end;
      // atualiza valor totao pedido
      oConnection.StartTransaction;
      Command.Close;
      Command.CommandText.Text:= ' update pedido set valorped = :valorped where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := iCodPed;
      Command.Params.ParamByName('valorped').AsFloat    := dValorTotPed;
      Command.Execute();
      oConnection.Commit;
      //
      for ObjPedidoPagto in PedidoEnt.listapedidopagto do
      begin
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into pedidopagto (codped, codseqpagto, codformpagto, valorpagto) '+
                                   ' values (:codped, :codseqpagto, :codformpagto, :valorpagto)';
        Command.Params.ParamByName('codped').AsInteger       := iCodPed;
        Command.Params.ParamByName('codseqpagto').AsInteger  := fGeraCodSeqPagto(iCodPed);
        Command.Params.ParamByName('codformpagto').AsInteger := ObjPedidoPagto.codformpagto;
        Command.Params.ParamByName('valorpagto').AsFloat     := ObjPedidoPagto.valorpagto;
        Command.Execute();
        oConnection.Commit;
      end;
      //
      Result := 200;
    except
      on E:Exception do
      begin
        oConnection.Rollback;
        Log('Error: '+e.Message);
        Result := 500;
      end;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TPedidoDAO.fGeraCodPed: integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open('select first 1 coalesce(codped,0)+1 codpednovo from pedido order by codped desc');
    if Query.IsEmpty then
      Result := 1
    else
      Result := Query.FieldByName('codpednovo').AsInteger;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TPedidoDAO.fGeraCodSeqProd(iCodPed: integer): integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text   := ' select first 1 coalesce(codseqprod,0)+1 codseqprodnovo '+
                        ' from pedidoprod '+
                        ' where codped = :codped '+
                        ' order by codseqprod desc';
    Query.ParamByName('codped').AsInteger := iCodPed;
    Query.Open;
    if Query.IsEmpty then
      Result := 1
    else
      Result := Query.FieldByName('codseqprodnovo').AsInteger;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TPedidoDAO.fGeraCodSeqPagto(iCodPed: integer): integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text   := ' select first 1 coalesce(codseqpagto,0)+1 codseqpagtonovo '+
                        ' from pedidopagto '+
                        ' where codped = :codped '+
                        ' order by codseqpagto desc';
    Query.ParamByName('codped').AsInteger := iCodPed;
    Query.Open;
    if Query.IsEmpty then
      Result := 1
    else
      Result := Query.FieldByName('codseqpagtonovo').AsInteger;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TPedidoDAO.fGeraCodSeqIng(iCodPed, iCodSeqProd: integer): integer;
var
  Query: TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text   := ' select first 1 coalesce(codseqing,0)+1 codseqingnovo '+
                        ' from pedidoproding '+
                        ' where codped = :codped and codseqprod = :codseqprod '+
                        ' order by codseqing desc';
    Query.ParamByName('codped').AsInteger     := iCodPed;
    Query.ParamByName('codseqprod').AsInteger := iCodSeqProd;
    Query.Open;
    if Query.IsEmpty then
      Result := 1
    else
      Result := Query.FieldByName('codseqingnovo').AsInteger;
  finally
    oConnection.Close;
    oConnection.Free
  end;
end;

function TPedidoDAO.AlteraPedido(PedidoEnt: TPedidoEnt): Integer;
var
  sParamDescRetItem: string;
  i, iCodPed, iCodSeqProd, iCodSeqIng: integer;
  dValorProd, dValorAcres, dValorDescres,
  dValorTotPed: Double;
  Command:TFDCommand;
  Query: TFDQuery;
  oConnection:TFDConnection;
  ObjPedidoProd:TPedidoProdEnt;
  ObjPedidoProdIng:TPedidoProdIngEnt;
  ObjPedidoProdAcao:TPedidoProdAcaoEnt;
  ObjPedidoPagto:TPedidoPagtoEnt;
begin
  try
    oConnection         := GetConnection;
    Command             := TFDCommand.Create(nil);
    Command.Connection  := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    try
      oConnection.StartTransaction;
      Command.Close;
      Command.CommandText.Text:= ' update pedido set codpes = :codpes, valorped = :valorped, viagem = :viagem, pago = :pago, '+
                                 '    datahora = :datahora, status = :status, obs = :obs '+
                                 ' where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Params.ParamByName('codpes').AsInteger    := PedidoEnt.codpes;
      Command.Params.ParamByName('valorped').AsFloat    := PedidoEnt.valorped;
      Command.Params.ParamByName('viagem').AsInteger    := PedidoEnt.viagem;
      Command.Params.ParamByName('pago').AsInteger      := PedidoEnt.pago;
      Command.Params.ParamByName('datahora').AsDateTime := Now;
      Command.Params.ParamByName('status').AsInteger    := PedidoEnt.status;
      Command.Params.ParamByName('obs').AsString        := PedidoEnt.obs;
      Command.Execute();
      oConnection.Commit;
      // Apaga tabelas filhas e netas
      Command.Close;
      Command.CommandText.Text:= ' delete from pedidoproding where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Execute();
      oConnection.Commit;
      //
      Command.Close;
      Command.CommandText.Text:= ' delete from pedidoprodacao where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Execute();
      oConnection.Commit;
      //
      Command.Close;
      Command.CommandText.Text:= ' delete from pedidoprod where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Execute();
      oConnection.Commit;
      //
      Command.Close;
      Command.CommandText.Text:= ' delete from pedidopagto where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Execute();
      oConnection.Commit;

      dValorTotPed := 0;

      // altera produtos do pedido
      for ObjPedidoProd in PedidoEnt.listapedidoprod do
      begin
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into pedidoprod (codped, codseqprod, codprod, valorunitprod, qtdprod, status, valortotprod) '+
                                   ' values (:codped, :codseqprod, :codprod, :valorunitprod, :qtdprod, :status, :valortotprod)';
        Command.Params.ParamByName('codped').AsInteger      := PedidoEnt.codped;
        //
        if ObjPedidoProd.codseqprod = 0 then
          iCodSeqProd := fGeraCodSeqProd(PedidoEnt.codped)
        else
          iCodSeqProd := ObjPedidoProd.codseqprod;
        //
        Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
        Command.Params.ParamByName('codprod').AsInteger     := ObjPedidoProd.codprod;
        Command.Params.ParamByName('valorunitprod').AsFloat := ObjPedidoProd.valorunitprod;
        Command.Params.ParamByName('qtdprod').AsInteger     := ObjPedidoProd.qtdprod;
        Command.Params.ParamByName('status').AsInteger      := ObjPedidoProd.status;
        //
        dValorProd := (ObjPedidoProd.valorunitprod * ObjPedidoProd.qtdprod);
        //
        Command.Execute();
        oConnection.Commit;

        // altera tabela de acoes do produto
        for ObjPedidoProdAcao in ObjPedidoProd.listapedidoprodacao do
        begin
          oConnection.StartTransaction;
          Command.Close;
          Command.CommandText.Text:= ' insert into pedidoprodacao (codped, codseqprod, codacao) '+
                                     ' values (:codped, :codseqprod, :codacao)';
          Command.Params.ParamByName('codped').AsInteger      := iCodPed;
          Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
          Command.Params.ParamByName('codacao').AsInteger     := ObjPedidoProdAcao.codacao;
          //
          Command.Execute();
          oConnection.Commit;
        end;

        // altera ingredientes do produto
        dValorAcres := 0;
        dValorDescres := 0;
        for ObjPedidoProdIng in ObjPedidoProd.listapedidoproding do
        begin
          oConnection.StartTransaction;
          Command.Close;
          Command.CommandText.Text:= ' insert into pedidoproding (codped, codseqprod, codseqing, codproding, valoruniting, qtdproding, ingpadrao) '+
                                     ' values (:codped, :codseqprod, :codseqing, :codproding, :valoruniting, :qtdproding, :ingpadrao)';
          Command.Params.ParamByName('codped').AsInteger      := PedidoEnt.codped;
          Command.Params.ParamByName('codseqprod').AsInteger  := iCodSeqProd;
          //
          if ObjPedidoProdIng.codseqing = 0 then
            iCodSeqIng := fGeraCodSeqIng(PedidoEnt.codped,iCodSeqProd)
          else
            iCodSeqIng := ObjPedidoProdIng.codseqing;
          //
          Command.Params.ParamByName('codseqing').AsInteger   := iCodSeqIng;
          Command.Params.ParamByName('codproding').AsInteger  := ObjPedidoProdIng.codproding;
          Command.Params.ParamByName('valoruniting').AsFloat  := ObjPedidoProdIng.valoruniting;
          Command.Params.ParamByName('qtdproding').AsInteger  := ObjPedidoProdIng.qtdproding;
          Command.Params.ParamByName('ingpadrao').AsInteger   := ObjPedidoProdIng.ingpadrao;
          Command.Execute();
          oConnection.Commit;
          //
          if (ObjPedidoProdIng.ingpadrao = 1) and (ObjPedidoProdIng.valoruniting > 0) then
            dValorAcres := dValorAcres + (ObjPedidoProdIng.valoruniting * ObjPedidoProdIng.qtdproding);

          try
             Query := TFDQuery.Create(nil);
             Query.Connection := oConnection;
             Query.Open(' select conteudo from parametros where descparams = ''DESCONTO_RETIRAR_ITEM'' ');
             sParamDescRetItem := Query.FieldByName('conteudo').AsString;
          finally
             FreeAndNil(Query);
          end;

          if (sParamDescRetItem = 'S') and (ObjPedidoProdIng.ingpadrao = 2) and (ObjPedidoProdIng.valoruniting > 0) then
            dValorDescres := dValorDescres + (ObjPedidoProdIng.valoruniting * ObjPedidoProdIng.qtdproding);
        end;
        //
        dValorTotPed := dValorTotPed + dValorProd + (dValorAcres * ObjPedidoProd.qtdprod);
        //
        // atualiza valor total do produto
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' update pedidoprod set valortotprod = :valortotprod, valoracres = :valoracres '+
                                   ' where codped = :codped and codseqprod = :codseqprod';
        Command.Params.ParamByName('codped').AsInteger     := PedidoEnt.codped;
        Command.Params.ParamByName('codseqprod').AsInteger := iCodSeqProd;
        Command.Params.ParamByName('valortotprod').AsFloat := dValorProd + (dValorAcres * ObjPedidoProd.qtdprod);
        Command.Params.ParamByName('valoracres').AsFloat   := dValorAcres * ObjPedidoProd.qtdprod;
        Command.Execute();
        oConnection.Commit;
      end;

      // atualiza valor total pedido
      oConnection.StartTransaction;
      Command.Close;
      Command.CommandText.Text:= ' update pedido set valorped = :valorped where codped = :codped';
      Command.Params.ParamByName('codped').AsInteger    := PedidoEnt.codped;
      Command.Params.ParamByName('valorped').AsFloat    := dValorTotPed;
      Command.Execute();
      oConnection.Commit;
      //
      // altera formas de pagamento do pedido
      for ObjPedidoPagto in PedidoEnt.listapedidopagto do
      begin
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into pedidopagto (codped, codseqpagto, codformpagto, valorpagto) '+
                                   ' values (:codped, :codseqpagto, :codformpagto, :valorpagto)';
        Command.Params.ParamByName('codped').AsInteger       := PedidoEnt.codped;
        Command.Params.ParamByName('codseqpagto').AsInteger  := fGeraCodSeqPagto(PedidoEnt.codped);
        Command.Params.ParamByName('codformpagto').AsInteger := ObjPedidoPagto.codformpagto;
        Command.Params.ParamByName('valorpagto').AsFloat     := ObjPedidoPagto.valorpagto;
        Command.Execute();
        oConnection.Commit;
      end;
      //
      Result := 200;
    except
      on E:Exception do
      begin
        oConnection.Rollback;
        Log('Error: '+e.Message);
        Result := 500;
      end;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TPedidoDAO.ExcluiPedido(PCodPed: Integer): integer;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from pedido where codped = '+IntToStr(PCodPed));
    if not Query.IsEmpty then
    begin
      try
        dmConexcao.FDConexao.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from pedidoprodacao where codped = :codped ';
        Command.Params.ParamByName('codped').AsInteger := PCodPed;
        Command.Execute();
        //
        Command.Close;
        Command.CommandText.Text := ' delete from pedidoproding where codped = :codped ';
        Command.Params.ParamByName('codped').AsInteger := PCodPed;
        Command.Execute();
        //
        Command.Close;
        Command.CommandText.Text := ' delete from pedidoprod where codped = :codped ';
        Command.Params.ParamByName('codped').AsInteger := PCodPed;
        Command.Execute();
        //
        Command.Close;
        Command.CommandText.Text := ' delete from pedidopagto where codped = :codped ';
        Command.Params.ParamByName('codped').AsInteger := PCodPed;
        Command.Execute();
        //
        Command.Close;
        Command.CommandText.Text := ' delete from pedido where codped = :codped ';
        Command.Params.ParamByName('codped').AsInteger := PCodPed;
        Command.Execute();
        dmConexcao.FDConexao.Commit;
        //
        Result := 200;
      except
        on E:Exception do
        begin
          dmConexcao.FDConexao.Rollback;
          Log('Error: '+e.Message);
          Result := 500;
        end;
      end;
    end
    else
    begin
      Log('Registro n�o encontrato na PRODUTOS');
      Result := 200;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

end.

